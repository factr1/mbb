<?php /* Template Name: Band Landing  */
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];

	get_header(); ?>


<div class="band-hero text-center hide-for-small-only" style="background:url(<?=$url?>) center center no-repeat;">
	<h1>The Band</h1>
	<h2>The Method Behind the Music</h2>
	<div class="members">
		<ul class="small-block-grid-2 medium-block-grid-6">

			<li><a data-scroll href="#michael">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/michael_benson.jpg">
					<div class="member-overlay">
						<p>MICHAEL BENSON</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#brian">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/brian-bio.jpg">
					<div class="member-overlay">
						<p>BRIAN MONRONEY</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#osama">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/osama_afifi.jpg">
					<div class="member-overlay">
						<p>OSAMA AFIFI</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#tony">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/tony_rondolone.jpg">
					<div class="member-overlay">
						<p>TONY RONDOLONE</p>
					</div>
				</div>
			</a></li>


			<li><a data-scroll href="#pete">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/pete.jpg">
					<div class="member-overlay">
						<p>PETE MOLNER</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#darelle">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/darelle_thumb.jpg">
					<div class="member-overlay">
						<p>DARELLE HOLDEN</p>
					</div>
				</div>
			</a></li>

		</ul>
	</div>
</div>

<!--mobile header for band page -->
<div class="mobile-band-hero show-for-small-only">
	<div class="band-hero text-center" style="background:url(<?=$url?>) center center no-repeat;">
		<h1>The Band</h1>
		<h2>The Method Behind The Music</h2>
	</div>
	<div class="members">
		<ul class="small-block-grid-2 medium-block-grid-6">

			<li><a data-scroll href="#michael">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/michael_benson.jpg">
					<div class="member-overlay">
						<p>MICHAEL BENSON</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#brian">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/brian-bio.jpg">
					<div class="member-overlay">
						<p>BRIAN MONRONEY</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#pete">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/pete.jpg">
					<div class="member-overlay">
						<p>PETE MOLNER</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#tony">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/tony_thumb.jpg">
					<div class="member-overlay">
						<p>TONY RONDOLONE</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#david">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/osama.jpg">
					<div class="member-overlay">
						<p>OSAMA AFIFI</p>
					</div>
				</div>
			</a></li>

			<li><a data-scroll href="#darelle">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/darelle_thumb.jpg">
					<div class="member-overlay">
						<p>DARELLE HOLDEN</p>
					</div>
				</div>
			</a></li>

		</ul>
	</div>
</div>
<!-- end mobile header for band page -->

<section class="member-bios">
	<div id="michael" class="bio-image" style="background:url(<?php the_field('mbb-background');?>) center center no-repeat;">
		<div class="bio">
			<div class="row">
				<div class="medium-6 columns">
					<?php if(get_field('instrument_icons', '51')):
									while(has_sub_field('instrument_icons', '51')): ?>
					<img src="<?php the_sub_field('icon', '51');?>">
					<?php endwhile; endif; ?>

					<h1><?php echo get_the_title( 51 );?></h1>
					<div class="member-image" class="show-for-small-only">
						<img src="<?php the_field('mobile_image', '51');?>">
					</div>
					<p>
						<?php the_field('short_bio', '51');?>
					</p>
					<div class="double-cols">
						<?php the_field('favorites', '51');?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="full-bar text-center bio-open">
		<p>FULL BIOGRAPHY <img src="<?php bloginfo('template_url');?>/images/down-arrow-bio.png" class="indicator"></p>
	</div>

	<div class="full-bio">
		<div class="row">
			<div class="medium-7 columns">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content( );?>
				<?php endwhile; endif;?>
			</div>
			<div class="medium-5 columns">
				<div id="bio-gallery">
					<?php if(get_field('images', '51')):
							while(has_sub_field('images', '51')): ?>
					<div>
							<img src="<?php the_sub_field('image', '51');?>">
					</div>
					<?php endwhile;endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php
		if(get_field('band_repeater')):
		while(has_sub_field('band_repeater')):
		?>
	<div id="<?php the_sub_field('id');?>" class="bio-image" style="background:url(<?php the_sub_field('bg_img');?>) left center no-repeat;">
		<div class="bio">
			<div class="row">
				<div class="medium-6 columns">
					<?php if(get_sub_field('icons')):
									while(has_sub_field('icons')): ?>
					<img src="<?php the_sub_field('icon');?>">
					<?php endwhile; endif; ?>

					<h1><?php the_sub_field('name');?></h1>
					<div class="member-image" class="show-for-small-only">
						<img src="<?php the_sub_field('member_image');?>">
					</div>
					<p>
						<?php the_sub_field('member_bio');?>
					</p>
					<div class="double-cols">
						<?php the_sub_field('favorites');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endwhile; endif; ?>
</section>


<?php get_footer();?>
