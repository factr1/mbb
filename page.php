<?php get_header(); ?>

<?php
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'full');
	$url = $thumb['0'];
	?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="page-header" style="background:url(<?=$url?>) center center no-repeat;">
	
	<div class="row">
		<div class="medium-12 text-center columns">
			<h1><?php the_title();?></h1>
		</div>
	</div>
</div>
<article id="single-page">
	<div class="row">
		<div class="medium-12">
			<?php the_content();?>
		</div>
	</div>
	<?php endwhile; endif; ?>
</article>



<?php get_footer(); ?>
