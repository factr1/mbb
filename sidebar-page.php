<div class="sidebar">
	<form method="get" id="searchform" action="<?php bloginfo('home');?>">
	<input type="search" placeholder="What are you looking for?" name="s">
	</form>
    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Page Sidebar')) : ?>
	    
	    
	    <a href="<?php bloginfo('url');?>/accolades/#share">
	<div class="share-story">
		SHARE YOUR STORY
	</div>
</a>
<div class="icon-container clearfix">
	<a href="<?php bloginfo('url');?>/music/">
	<div class="icon-single">
		<img src="<?php bloginfo('template_url');?>/images/listen.png">
			<br>
		<p>LISTEN</p>
	</div>
	</a>
	<a href="<?php bloginfo('url');?>/music/#videos">
	<div class="icon-single">
		<img src="<?php bloginfo('template_url');?>/images/watch.png">
			<br>
		<p>WATCH</p>
	</div>
	</a>
</div>

<a href="<?php bloginfo('url');?>/calendar/">
<div class="calendar-callout clearfix">
	<div class="cal-left">
		<img src="<?php bloginfo('template_url');?>/images/calendar-blue.png">
	</div>
	<div class="cal-right">
		<p>CHECK OUR BOOKED EVENT CALENDAR</p>
	</div>
</div>
</a>
	    
	    
	    <? else : ?>
    
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->

		<h1>Page sidebar</h1>
    	<h2>Archives</h2>
    	<ul>
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
        
        <h2>Categories</h2>
        <ul>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
        </ul>
        
    	<?php wp_list_bookmarks(); ?>
    
    	</ul>
    	
    	<h2>Subscribe</h2>
    	<ul>
    		<li><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
    		<li><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
    	</ul>
	
	<?php endif; ?>

</div>