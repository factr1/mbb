<? /*Template Name: Calendar */

/**
 * Query Upcoming Events
 */
$arrRecentEvents = array();
$today = date('Y-m-d');
$args = array(
    'post_type' => 'event',
    'posts_per_page' => 4,
    'orderby' => 'meta_value',
    'meta_key' => '_event_start_date',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => '_event_start_date',
            'value' => $today,
            'compare' => '>=',
        )
    )
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while($query->have_posts()) {
      $query->the_post();
      $arrRecentEvents[] = $post; // add post to recent posts array
    }
}
wp_reset_postdata(); // reset original post data

	get_header();?>
	
<!-- accolades page header -->
<section class="music-page">
	<div class="event-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>EVENT CALENDAR</h1>
				<h2>Book your date today</h2>
			</div>
		</div>
		
	</div>
	
	
	<div class="calendar">
		<div class="row">
			<div class="medium-10 columns medium-centered calendar-grid">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<?php the_content();?>
				
				<? endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>	
<?php get_footer();?>