<?php get_header(); ?>

<div class="cover-404">
<div class="row">
	<div class="medium-10 medium-centered text-center columns">
		<h1>Error 404</h1>
		<p>Oh no! Something went wrong. The page you were trying to find has either moved, or does not exist.</p>
	</div>
</div>
</div>



