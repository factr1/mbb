<?php /*Template Name: Weddings */
	get_header();
	//get the featured image
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];
	?>
	
<div class="corp-header" style="background:url(<?=$url?>) center center no-repeat;">
	<div class="row">
		<div class="medium-10 columns medium-centered text-center">
			<h1><?php the_title();?></h1>
			<h2>NEVER STOP LOVING, NEVER STOP DANCING</h2>
		</div>
	</div>
	<div class="corp-subheader">
		<div class="row">
			<div class="medium-10 columns medium-centered text-center">
				<p><?php the_field('quote');?></p>
				<p class="cite"><?php the_field('cite');?></p>
			</div>
		</div>
	</div>
</div>
<div class="corp-white">
	<div class="row">
		<div class="medium-5 columns">
			<h1>EVERY DETAIL, EVERY TIME.</h1>
		</div>
	</div>
	<div class="row">
		<div class="medium-12 columns two-col-content">
			<?php if (have_posts()) : while (have_posts()) : the_post();
					
					
				the_content( );
					
					
				endwhile; endif;?>
		</div>
		<hr>
		<div class="medium-12 columns">
			<?php the_field('content_append');?>
		</div>
	</div>
</div>
<div class="corp-blue">
	<div class="row">
		<div class="medium-12 columns three-col">
			<?php the_field('services');?>
		</div>
	</div>
</div>
	
<? get_footer();?>