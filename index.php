<?php get_header();?>

<!-- accolades page header -->
<section class="music-page">
	<div class="blog-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>BLOG</h1>
			</div>
		</div>
	</div>
	
	
	<div class="blog-section">
		<div class="row">
			<div class="small-11 small-centered medium-uncentered medium-8 columns">
				<ul class="small-block-grid-1 medium-block-grid-2 posts">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),'blog');
					$url = $thumb['0'];
					?>

					<li>
						<div class="featured-image-blog" style="background:url(<?=$url?>) center center no-repeat; background-size: cover;">
							<div class="overlay">
								<div class="date-circle"><span><?php the_time('M');?></span><h3><?php the_time('j');?></h3><span><?php the_time('Y');?></span></div>
								<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
								<span>In: <?php the_category(' '); ?></span>
							</div>
						</div>
					</li>
					<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
										
				</ul>
			</div>
			
			<div class="medium-4 columns sidebar">
				<?php get_sidebar('page'); ?>
			</div>
		</div>
	</div>
	
<?php get_footer();?>
