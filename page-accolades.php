<?php /* Template Name: Accolades */ 
	get_header();?>

<!-- accolades page header -->
<section class="music-page">
	<div class="accolades-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>ACCOLADES</h1>
				<h2>WE WIN WHEN YOU WIN.</h2>
			</div>
		</div>
		<div class="awards hide-for-small">
			<div class="row">
				<div class="small-4 medium-3 columns">
					<img src="<?php the_field('award_image');?>">
				</div>
				<div class="small-8 medium-9 columns">
					<h3><?php the_field('award_heading');?></h3>
					<p><?php the_field('award_text');?></p>
				</div>
			</div>
		</div>
	</div>
	<!--mobile only award section-->
	<div class="row show-for-small-only awards-mobile">
		<div class="small-5 small-centered columns">
			<img src="<?php the_field('award_image');?>">
		</div>
		<div class="small-12 columns">
			<h3><?php the_field('award_heading');?></h3>
					<p><?php the_field('award_text');?></p>

		</div>
	</div>
	
	<div class="client-section">
		<div class="row">
			<div class="medium-9 columns">
				<h3>CLIENT RAVES</h3>
				<div class="row">
					<div class="medium-12 columns">
						
						<?php
						wp_reset_postdata();
						global $post;
						$newsargs = array( 'numberposts' => 3, 'category_name' => 'testimonials' );
						$newsposts = get_posts( $newsargs );
						foreach( $newsposts as $post ) :	setup_postdata($post); ?>
						<p><?php 
								$content = get_the_content();
								$trimmed_content = wp_trim_words(strip_shortcodes(get_the_content()), 50);
								echo $trimmed_content;?></p>
						<a href="<?php permalink_link(); ?>" class="button button-small">Read More</a>
						
						<? endforeach; wp_reset_postdata();?>
						
					</div>
					<div class="medium-12 columns text-center">
						<a href="<? bloginfo('url');?>/category/testimonials/" class="button">VIEW ALL</a>
					</div>
				</div>
			</div>
			<div class="medium-3 columns text-center references">
				<a href="<?php the_field('wedding_references');?>">
					<img src="<?php bloginfo('template_url');?>/images/down-arrow.png">
					<p>DOWNLOAD A PDF OF WEDDING REFERENCES</p>
				</a>
					<br><br>
				<a href="<?php the_field('corporate_references');?>">
					<img src="<?php bloginfo('template_url');?>/images/down-arrow.png">
					<p>DOWNLOAD A PDF OF CORPORATE REFERENCES</p>
				</a>
			</div>
		</div>
		
	</div>
	
	<div id="share" class="share">
		<div class="row circle-row">
			<div class="medium-9 columns medium-centered">
				<h2>SHARE YOUR EXPERIENCE</h2>
				
				<?php echo do_shortcode('[gravityform id=2 title=false description=false]');?>
				
			</div>
		</div>
	</div>
	
	
<?php get_footer();?>