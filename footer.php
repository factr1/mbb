
	<!-- BEGIN BOOK NOW / SOCIAL MEDIA SECTION -->
    <div id="sixth-section">
        <div class="row">
            <div class="medium-12 columns">
                <h1>
                    HAVE THE BEST NIGHT OF YOUR LIFE.
                </h1>
	                <div class="medium-4 small-centered columns">
	                	<a href="<?php bloginfo('url');?>/contact/" class="button [large]">BOOK NOW</a>
	            	</div>
            </div>
        </div>
        
        <div class="row show-for-small-only">
            <div class="small-4 columns social-media">
                <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
            </div>
            <div class="small-4 columns social-media">
                <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
            </div>
            <div class="small-4 columns social-media social-media-last">
                <a href="#"><i class="fa fa-instagram fa-2x"></i></a>
            </div>
        </div>
        
        <div class="row show-for-medium-up">
            <div class="small-4 medium-4 columns social-media">
                <a href="<?php the_field('facebook','options');?>" target="_blank"><i class="fa fa-facebook fa-2x float-left"></i><span>LIKE US ON FACEBOOK</span></a>
            </div>
            <div class="small-4 medium-4 columns social-media">
                <a href="<?php the_field('twitter','options');?>" target="_blank"><i class="fa fa-twitter fa-2x float-left"></i><span>FOLLOW US ON TWITTER</span></a>
            </div>
            <div class="small-4 medium-4 columns social-media social-media-last">
                <a href="<?php the_field('instagram','options');?>" target="_blank"><i class="fa fa-instagram fa-2x float-left"></i><span>SEE US ON INSTAGRAM</span></a>
            </div>
        </div>

    
    <!-- BEGIN FOOTER -->
		<footer>
			<div class="row">
				<div class="small-5 medium-1 columns">
					<img src="<?php bloginfo('template_url');?>/images/small-block-logo.png" alt="Small Michael Benson Logo">
				</div>
				<div class="small-7 medium-7 columns">
					<small class="text-left">
						© Copyright 2014 Michael Benson Band. All rights reserved.
					</small>
				</div>
				<div class="medium-4 mobile-footer columns">
					<div class="row mobile-footer-padding">
						<div class="small-12 medium-12 columns">
							<span>DESIGNED BY:</span> <img src="<?php bloginfo('template_url');?>/images/m-logo.png" alt="MINDBOGL Logo" class="footer-img">
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns">
							<span>DEVELOPED BY:</span><span class="factor1">factor1</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
	 
	</div>
<!-- </section> -->



	<script type="text/javascript">
	  jQuery(document).ready(function($) {
			$('#bio-gallery').justifiedGallery({
				sizeRangeSuffixes : {'lt100':'', 
									'lt240':'', 
									'lt320':'', 
									'lt500':'', 
									'lt640':'', 
									'lt1024':''},
				margins	: 2,
				rowHeight: 200,
				lastRow: 'nojustify',
			});
	 });
	</script>

	
	<script>
	smoothScroll.init(); //smooth scroll	
		
	$(document).ready(function(){
		$('.award-slider').slick({
		autoplay: true,
		arrows: false,
		});
	});
	</script>
	
	
	<? wp_footer();?>

</body>
</html>