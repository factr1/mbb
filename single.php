<?php get_header(); 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];	
?>
<?php if(has_post_thumbnail()) { ?>
<div class="single-header" style="background:url(<?=$url?>) center center no-repeat;">
	<?php } else { ?>
		<div class="single-header" style="background: url(images/crowdsurf.jpg) center center no-repeat;">
	<? } ?>
<div class="row">
	<div class="medium-10 medium-centered text-center columns">
		<h1 style="display:<? the_field('title_toggle');?>"><?php the_title();?></h1>
	</div>
</div>	

</div>

<article class="single-row">
<div class="row">
	<div class="medium-8 columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<h2 class="blog-title"><?php the_title(); ?></h2>
			<div style="display:<? the_field('meta_toggle');?>"><?php include (TEMPLATEPATH . '/inc/meta.php' ); ?></div>
			
	
			
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php the_tags( 'Tags: ', ', ', ''); ?>


			
			<?php edit_post_link('Edit this entry','','.'); ?>
			
		</div>

	<?php comments_template(); ?>

	<?php endwhile; endif; ?>
	</div>

	<div class="medium-4 columns sidebar">	
		<?php get_sidebar(); ?>
	</div>
</div>
</article>

<?php get_footer(); ?>

