<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header(); ?>
<section class="music-page">
	<div class="event-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>EVENT CALENDAR</h1>
			</div>
		</div>
		<div class="calendar-row">
			<div class="row">
				<div class="medium-6 medium-centered columns event-when text-left">
					<h3>WHEN IS YOUR EVENT?</h3> <img src="<?php bloginfo('template_url');?>/images/calendar.png">
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="calendar">
		<div class="row">
			<div class="medium-10 columns medium-centered calendar-grid">
				<div id="tribe-events-pg-template">
					<?php tribe_events_before_html(); ?>
					<?php tribe_get_view(); ?>
					<?php tribe_events_after_html(); ?>
				</div> <!-- #tribe-events-pg-template -->
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>