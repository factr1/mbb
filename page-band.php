<?php /* Template Name: Band  */
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];

	get_header(); ?>


<div class="band-hero text-center hide-for-small-only" style="background:url(<?=$url?>) center center no-repeat;">
	<h1>The Band</h1>
	<h2>The Method Behind the Music</h2>
	<div class="members">
		<ul class="small-block-grid-2 medium-block-grid-6">
			<li><a data-scroll href="#michael">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/michael_benson.jpg">
					<div class="member-overlay">
						<p>MICHAEL BENSON</p>
					</div>
				</div>
			</a></li>
			<li><a data-scroll href="#michael-e">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/brian-bio.jpg">
					<div class="member-overlay">
						<p>BRIAN MONRONEY</p>
					</div>
				</div>
			</a></li>
			<li><a data-scroll href="#pete">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/pete.jpg">
					<div class="member-overlay">
						<p>PETE MOLNER</p>
					</div>
				</div>
			</a></li>
			<li><a data-scroll href="#dewey">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/sax.jpg">
					<div class="member-overlay">
						<p>DEWEY MARLER</p>
					</div>
				</div>
			</a></li>
			<li><a data-scroll href="#osama">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/osama.jpg">
					<div class="member-overlay">
						<p>OSAMA AFIFI</p>
					</div>
				</div>
			</a></li>
			<li><a data-scroll href="#darelle">
				<div class="member-container">
					<img src="<?php bloginfo('template_url');?>/images/darelle_thumb.jpg">
					<div class="member-overlay">
						<p>DARELLE HOLDEN</p>
					</div>
				</div>
			</a></li>
		</ul>

	</div>
</div>

<!--mobile header for band page -->
<div class="show-for-small-only band-hero">
	<h1>The Band</h1>
	<h2>The Method Behind The Music</h2>
</div>
<!-- //end mobile header for band page -->

<section class="member-bios">
	<div class="bio-image" style="background:url(<?php bloginfo('template_url');?>/images/mb.jpg) left center no-repeat;">
		<div class="bio">
			<div class="row">
				<div class="medium-6 columns">
					<?php if(get_field('instrument_icons')):
									while(has_sub_field('instrument_icons')): ?>
					<img src="<?php the_sub_field('icon');?>">
					<?php endwhile; endif; ?>

					<h1><?php the_title();?></h1>
					<p>
						<?php the_field('short_bio');?>
					</p>
					<div class="double-cols">
						<?php the_field('favorites');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="full-bar text-center bio-open">
		<p>FULL BIOGRAPHY <img src="<?php bloginfo('template_url');?>/images/down-arrow-bio.png" class="indicator"></p>
	</div>
	<div class="full-bio">
		<div class="row">
			<div class="medium-7 columns">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content();?>
				<?php endwhile; endif;?>
			</div>
			<div class="medium-5 columns">
				<div id="bio-gallery">
					<?php if(get_field('images')):
							while(has_sub_field('images')): ?>
					<div>
							<img src="<?php the_sub_field('image');?>">
					</div>
					<?php endwhile;endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer();?>
