<?php  /*Template Name: Music */
	get_header();?>

<!-- music page header -->
<section class="music-page">
	<div class="music-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>MUSIC</h1>
				<h2>SINGING ALONG IS ENCOURAGED.</h2>
			</div>
		</div>
		<div class="row circle-row">
			<div class="circle-first small-4 medium-3 columns">
				<a data-scroll href="#music">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/music-note.png">
				</div>
				</a>
			</div>
			<div class="small-4 medium-3 columns">
				<a data-scroll href="#list">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/bullets.png" class="bullets">
				</div>
				</a>
			</div>
			<div class="small-4 medium-3 columns end">
				<a data-scroll href="#videos">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/video.png" class="video">
				</div>
				</a>
			</div>
		</div>
	</div>

	<div id="music" class="audio-section">
		<div class="row">
			<div class="medium-11 medium-centered columns">
				<div class="blue-circle">
					<img src="<?php bloginfo('template_url');?>/images/blue-music-note.png">
				</div>
				<h1 class="blue-text">PLAYLISTS</h1>
				<p class="blue-text quote"><?php the_field('client_quote');?></p>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 medium-centered columns">
				<div id="player1" class="audio-player">
					<?php the_field('player_1');?>
				</div>
				<?php wp_nav_menu( array( 'theme_location' => 'Playlists' ) ); ?>
			</div>
		</div>
	</div>

	<div id="list" class="song-list">
		<div class="row circle-row">
			<div class="medium-11 columns medium-centered">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/bullets.png" class="bullets">
				</div>
				<h1>SONG LISTS</h1>
					<p>
						<?php the_field('client_quote_2');?>
					</p>

					<div id="player2">
						<?php the_field('player_2');?>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-6 medium-centered columns text-center">
				<a href="<?php the_field('song_list');?>" target="_blank" class="pdf">
				<img src="<?php bloginfo('template_url');?>/images/down-arrow.png">
					<br><br>
				<span>DOWNLOAD SONG LIST PDF</span>
				</a>
			</div>
		</div>
	</div>

	<div id="videos" class="videos">
		<div class="row">
			<div class="medium-11 medium-centered columns">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/video.png" class="video">
				</div>
				<h1>VIDEOS</h1>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 columns">
				<div class="fluidMedia margin-bottom">
					<?php the_field( 'featured_video' ); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-6 columns">
				<div class="fluidMedia margin-bottom">
					<?php the_field('left_video');?>
				</div>
			</div>
			<div class="medium-6 columns">
				<div class="fluidMedia">
					<?php the_field('right_video');?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer();?>
