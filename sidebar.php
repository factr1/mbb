<form method="get" id="searchform" action="<?php bloginfo('home');?>">
<input type="search" placeholder="What are you looking for?" name="s">
</form>
<a href="#">
	<div class="wedding-client-raves">
		WEDDING CLIENT RAVES
	</div>
</a>
<a href="#">
	<div class="corporate-client-raves">
		CORPORATE CLIENT RAVES
	</div>
</a>
<a href="#">
	<div class="colleague-raves">
		COLLEAGUE RAVES
	</div>
</a>
<a href="#">
	<div class="from-the-band">
		FROM THE BAND
	</div>
</a>
<a href="<?php bloginfo('url');?>/accolades/#share">
	<div class="share-story">
		SHARE YOUR STORY
	</div>
</a>
<div class="icon-container clearfix">
	<a href="<?php bloginfo('url');?>/music/">
	<div class="icon-single">
		<img src="<?php bloginfo('template_url');?>/images/listen.png">
			<br>
		<p>LISTEN</p>
	</div>
	</a>
	<a href="<?php bloginfo('url');?>/music/#videos">
	<div class="icon-single">
		<img src="<?php bloginfo('template_url');?>/images/watch.png">
			<br>
		<p>WATCH</p>
	</div>
	</a>
</div>

<a href="<?php bloginfo('url');?>/calendar/">
<div class="calendar-callout clearfix">
	<div class="cal-left">
		<img src="<?php bloginfo('template_url');?>/images/calendar-blue.png">
	</div>
	<div class="cal-right">
		<p>CHECK OUR BOOKED EVENT CALENDAR</p>
	</div>
</div>
</a>