const gulp = require('gulp');
const sass = require('gulp-sass');
const nano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

const path = {
  scssMain: ['./main.scss'],
  scss: ['./css/*.scss'],
  cssMain: './',
  css: './css'
}

// Compile Main Sass File
gulp.task('main-sass', () => {
  return gulp.src( path.scssMain )
    .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass()
        .on('error', sass.logError)
        .on('error', notify.onError("Error compiling scss!"))
      )
      .pipe(autoprefixer({
        browsers: ['last 3 versions', 'Safari > 7'],
        cascade: false
      }))
    .pipe(nano({
      discardComments: {removeAll: true},
      autoprefixer: false,
      discardUnused: false,
      mergeIdents: false,
      reduceIdents: false,
      calc: {mediaQueries: true},
      zindex: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest( path.cssMain ))
});

// Compile Vendor Sass File
gulp.task('vendor-sass', () => {
  return gulp.src( path.scss )
    .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass()
        .on('error', sass.logError)
        .on('error', notify.onError("Error compiling scss!"))
      )
      .pipe(autoprefixer({
        browsers: ['last 3 versions', 'Safari > 7'],
        cascade: false
      }))
    .pipe(nano({
      discardComments: {removeAll: true},
      autoprefixer: false,
      discardUnused: false,
      mergeIdents: false,
      reduceIdents: false,
      calc: {mediaQueries: true},
      zindex: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest( path.css ))
});

gulp.task('styles', ['main-sass', 'vendor-sass']);

gulp.task('default', ['styles']);
