
	
		<!--static nav for all pages without video header -->
<div class="page-nav">
	<div class="row">
            <div class="small-7 medium-4 columns">
                    <a href="index.html"><img src="images/mb-logo.png" alt="Michael Benson Band Logo" class="show-for-medium-up" /></a>
                    <a href="index.html"><img src="images/small-block-logo.png" alt="Michael Benson Band Logo" class="show-for-small-only" /></a>
            </div>
            <div class="small-5 medium-8 large-8 columns">
                <nav class="menu show-for-large-up">
                    <ul>
                        <li><a href="#">THE BAND</a></li>
                        <li><a href="#">LISTEN</a></li>
                        <li><a href="accolades.html">TESTIMONIALS</a></li>
                        <li><a href="#">SERVICES</a></li>
                        <li><a href="calendar.html">CALENDAR</a></li>
                        <li><a href="blog.html">BLOG</a></li>
                        <li><a href="#">CONTACT</a></li>
                    </ul>
                </nav>
                <a href="#menu2" class="menu-link show-for-medium-down"> MENU &#9776;</a>
            </div>
        </div>
</div>

<!-- accolades page header -->
<section class="music-page">
	<div class="blog-header">
		<div class="row">
			<div class="medium-10 columns medium-centered  text-center">
				<h1>BLOG</h1>
			</div>
		</div>
	</div>
	
	
	<div class="blog-section">
		<div class="row">
			<div class="small-11 small-centered medium-uncentered medium-8 columns">
				<ul class="small-block-grid-1 medium-block-grid-2 posts">
					<li>
						<div class="featured-image-blog" style="background:url(http://placehold.it/300x300);">
							<div class="overlay">
								<div class="date-circle"><span>NOV</span><h3>10</h3><span>2012</span></div>
								<h3>STANDING OUT IN NASHVILLE</h3>
								<span>In: Professionals</span>
							</div>
						</div>
					</li>
					<li>
						<div class="featured-image-blog" style="background:url(http://placehold.it/300x300);">
							<div class="overlay">
								<div class="date-circle"><span>NOV</span><h3>10</h3><span>2012</span></div>
								<h3>NIGHT AND DAY</h3>
								<span>In: Professionals</span>
							</div>
						</div>
					</li>
					<li>
						<div class="featured-image-blog" style="background:url(http://placehold.it/300x300);">
							<div class="overlay">
								<div class="date-circle"><span>NOV</span><h3>10</h3><span>2012</span></div>
								<h3>TO NEW HEIGHTS</h3>
								<span>In: Professionals</span>
							</div>
						</div>
					</li>
					<li>
						<div class="featured-image-blog" style="background:url(http://placehold.it/300x300);">
							<div class="overlay">
								<div class="date-circle"><span>NOV</span><h3>10</h3><span>2012</span></div>
								<h3>MADE OUR NIGHT</h3>
								<span>In: Professionals</span>
							</div>
						</div>
					</li>
					
				</ul>
			</div>
			
			<div class="medium-4 columns sidebar">
				<input type="search" placeholder="What are you looking for?" name="search">
				
				<? get_sidebar('sidebar-page'); ?>
				
				<div class="icon-container">
					<div class="listen">
						<img src="<?php bloginfo('template_url');?>/images/listen.png">
							<br>
						<span>LISTEN</span>
					</div>
					<div class="listen">
						<img src="<?php bloginfo('template_url');?>/images/watch.png">
							<br>
						<span>WATCH</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
