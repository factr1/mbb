 <?php /* Template Name: Home Page */

 /**
 * Query Upcoming Events
 */
$arrRecentEvents = array();
$today = date('Y-m-d');
$args = array(
    'post_type' => 'event',
    'posts_per_page' => 4,
    'orderby' => 'meta_value',
    'meta_key' => '_event_start_date',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => '_event_start_date',
            'value' => $today,
            'compare' => '>=',
        )
    )
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while($query->have_posts()) {
      $query->the_post();
      $arrRecentEvents[] = $post; // add post to recent posts array
    }
}
wp_reset_postdata(); // reset original post data

 		get_header(); ?>

 <div class="show-for-small-only static-background">
		<div class="row">
		    <div class="small-12 small-centered columns square-logo text-center">
	            <img src="<?php bloginfo('template_url');?>/images/block-logo.png" alt="Michael Benson Band Logo" class="show-for-small-only" />

	        </div>
		</div>
    </div>

	<div class="row tall">
            <div class="large-10 large-centered medium-10 medium-centered hide-for-small-only columns square-logo center">
                <img src="<?php bloginfo('template_url');?>/images/block-logo.png" alt="Michael Benson Band Logo" />
            </div>
            <div class="show-more">
	             <a data-scroll href="#second-section"><i class="fa fa-chevron-down fa-2x"></i></a>
            </div>
    </div>

        <video id="bgvid" class="covervid-video hide-for-small-only" autoplay loop muted poster="images/screenshot.png">

        <source src="<?php bloginfo('template_url');?>/video/MBB_BROLL.mp4" type="video/mp4">

        </video>

		 <i class="fa fa-chevron-down hide-for-small-only"></i>

    <!-- BEGIN SECTION WITH SLIDER / AWARDS -->
    <div id="second-section">
        <a id="showHere"></a>
        <div class="row">
            <div class="medium-12 medium-centered columns">
                <h2>SEATTLE'S MOST <span class="bold">AWARD WINNING</span> BAND</h2>
            </div>
        </div>
        <div class="row award-slider">
	        <?php if(get_field('award_slider')):
		    		while(has_sub_field('award_slider')): ?>
            <div class="medium-12 columns">
                <img src="<?php the_sub_field('award_image');?>" alt="Awards Graphic" />
            </div>
            <?php endwhile; endif; ?>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <h2>
                    OUR CONSISTENCY HAS BECOME A TRADITION.
                </h2>
            </div>
        </div>
    </div>

    <!-- BEGIN LISTEN TO THIS SECTION -->
    <div id="third-section">
        <div class="row">
            <div class="medium-6 columns listen-text">
                <h2 class="text-left"><?php the_field('listen_headline');?></h2>
                <p class="text-left"><?php the_field('listen_text');?></p>
            </div>
            <div class="medium-5 columns music">
	            <!-- The jPlayer div must not be hidden. Keep it at the root of the body element to avoid any such problems. -->
				<div id="jquery_jplayer_1" class="cp-jplayer"></div>

				<!-- The container for the interface can go where you want to display it. Show and hide it as you need. -->

				<div id="cp_container_1" class="cp-container">
					<div class="cp-buffer-holder"> <!-- .cp-gt50 only needed when buffer is > than 50% -->
						<div class="cp-buffer-1"></div>
						<div class="cp-buffer-2"></div>
					</div>
					<div class="cp-progress-holder"> <!-- .cp-gt50 only needed when progress is > than 50% -->
						<div class="cp-progress-1"></div>
						<div class="cp-progress-2"></div>
					</div>
					<div class="cp-circle-control"></div>
					<ul class="cp-controls">
						<li><a class="cp-play" tabindex="1">play</a></li>
						<li><a class="cp-pause" style="display:none;" tabindex="1">pause</a></li> <!-- Needs the inline style here, or jQuery.show() uses display:inline instead of display:block -->
					</ul>
				</div>

                <!-- <img src="//<?php bloginfo('template_url');?>/images/play-btn.png" alt="play-btn"> -->
            </div>
        </div>
        <div class="row">
            <div class="medium-4 columns">
                <p class="text-left">

                </p>
            </div>
        </div>
    </div>

    <!-- BEGIN TESTIMONIAL SECTION -->
    <div id="fourth-section">
        <div class="row">
            <div class="large-12 large-centered columns">
                <h1 class="blue-text">
                    GUESTS TELL ALL.
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 medium-centered columns">
                <div class="flex-video widescreen vimeo">
                    <?php the_field('video_embed');?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-10 small-centered columns">
                <p>
                    <?php the_field('text_box_1');?>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns">
                <p>
                   <?php the_field('text_box_2');?>
                </p>
            </div>
        </div>
    </div>

    <!-- BEGIN UPCOMING EVENTS SECTION -->
    <div id="fifth-section">
        <div class="row">
            <div class="medium-12 columns text-center">
                <h1 class="blue-text">
                    UPCOMING EVENTS
                </h1>
                <h4 class="event-subheader">
                    <small class="blue-text">Dates fill fast!  View our calendar to see if your date is available.</small>
                </h4>
                <div class="calendar">
					<div class="row">
						<div class="medium-10 columns medium-centered calendar-grid text-left">

							<?php echo do_shortcode('[ai1ec view="agenda" events_limit="5"]');?>

						</div>
					</div>
				</div>
            </div>
        </div>
    </div>

<?php get_footer();?>
