<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie ie9"> <![endif]-->
<!--[if gt IE 9]>  <html lang="en" class="no-js ie">     <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js">    <!--<![endif]-->
<head>
    <META HTTP-EQUIV="personal" CONTENT="NO-CACHE" />
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <title>Michael Benson Band</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-iphone4.png" />
    
    <link type="text/css" rel="stylesheet" href="http://cdn.jsdelivr.net/foundation/5.0.2/css/foundation.min.css">
    <link type="text/css" rel="stylesheet" href="http://cdn.jsdelivr.net/foundation/5.0.2/css/normalize.css" />
    <link type="text/css" rel="stylesheet"  href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
     <?php wp_head(); ?>
   
   
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/f1-slide.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/f1-nav.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.justifiedGallery.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/slick.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/smooth-scroll.min.js"></script>
    
   
    <?php 
if ( is_page_template('page-home.php') ) { ?>
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.jplayer.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/circle.player.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.grab.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.transform2d.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/mod.csstransforms.min.js"></script>
    
     <script type="text/javascript">
//<![CDATA[

$(document).ready(function(){

	var myCirclePlayer = new CirclePlayer("#jquery_jplayer_1",
	{
		mp3: "http://www.michaelbensonband.com/wp-content/themes/mbb/mbb.mp3",
		m4a: "http://www.michaelbensonband.com/wp-content/themes/mbb/mbb.m4a",
		oga: "http://www.michaelbensonband.com/wp-content/themes/mbb/mbb.ogg"
	}, {
		cssSelectorAncestor: "#cp_container_1",
		swfPath: "../../dist/jplayer",
		wmode: "window",
		keyEnabled: true
	});
});
//]]>
</script>
	
<?php } ?>
    
     
   
     
   
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/slick.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/js/justifiedGallery.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/skin/circle.player/circle.player.css" />
    
   
	
</head>
<body <?php body_class();?>>
	<!-- OFF CANVAS F1 NAV -->
    <div id="menu-slide" class="panel">
	    <div class="row">
		    <div class="small-12 small-centered columns text-center">
			    <a href="<?php bloginfo('url');?>/">
			    	<img src="<?php bloginfo('template_url');?>/images/block-logo.png" class="Michael Benson Band Logo">
			    </a>
			    
			   
				    <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			    
			</div>
	    </div>
    </div>

  
    

    <header>
       <!--static nav for all pages without video header -->
<div class="page-nav">
	<div class="row">
            <div class="small-7 medium-4 columns">
                    <a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_url');?>/images/mb-logo.png" alt="Michael Benson Band Logo" class="show-for-medium-up" /></a>
                    <a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_url');?>/images/small-block-logo.png" alt="Michael Benson Band Logo" class="show-for-small-only" /></a>
            </div>
            <div class="small-5 medium-8 large-8 columns">
                <nav class="menu show-for-large-up">
                   <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                </nav>
                <a data-scroll href="#menu-slide" class="mm_open menu-link show-for-medium-down"> MENU &#9776;</a>
            </div>
        </div>
</div>
                       
    </header>
    
     