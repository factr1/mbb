<?php /*Template Name: FAQ */
	get_header();	
	
	//get the featured image
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];
	?>

<div class="faq-header" style="background: url(<?=$url?>) center center no-repeat;">
	<div class="row">
		<div class="medium-10 medium-centered text-center columns">
			<h1><?php the_title();?></h1>
		</div>
	</div>
</div>
<section class="faq">
	<div class="row">
		<div class="medium-10 medium-centered" style="margin: 0 auto;">
			<?php if (have_posts()) : while (have_posts()) : the_post();
					
					
				the_content( );
					
					
				endwhile; endif;?>
		</div>
	</div>
</section>	
	
<?php get_footer();?>