<?php get_header(); ?>
<div class="search">
	<div class="search-container">
		<div class="row">
			<div class="medium-10 medium-centered columns">
					<?php if (have_posts()) : ?>
				
						<h2>Search Results</h2>
				
						<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
				
						<?php while (have_posts()) : the_post(); ?>
				
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
				
				<?php the_excerpt(); ?>
				
				</div>
				
						<?php endwhile; ?>
				
						<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
				
					<?php else : ?>
				
						<h2>Sorry, nothing was found..</h2>
				
					<?php endif; ?>
			
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
