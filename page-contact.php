<?php /* Template Name: Contact */
	get_header();
	//get the featured image
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];		
	?>

<div class="contact-header" style="background:url(<?=$url?>) center center no-repeat;">
	<div class="row">
		<div class="medium-10 medium-centered text-center columns">
			<h1><?php the_title();?></h1>
			<h2>LET'S GET THIS PARTY STARTED</h2>
		</div>
	</div>
</div>
<section class="contact">
	<div class="row">
		<div class="medium-8 columns">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content();?>
			<?php endwhile; endif;?>
			<?php echo do_shortcode('[gravityform id=1 title=false description=false]'); ?>
		</div>
		<div class="medium-4 columns contact-side">
			<i class="fa fa-map-marker"></i> <p><?php the_field('address');?></p>
				<br>
			<i class="fa fa-phone"></i> <p><?php the_field('phone');?></p>
				<br>
			<i class="fa fa-mobile" style="font-size:2em !important;"></i> <p><?php the_field('mobile');?></p>
				<br>
			<i class="fa fa-envelope"></i> <p><a href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a></p>
			<?the_field('misc');?>
		</div>
	</div>
</section>	
<?php get_footer();?>