<?php /* Template Name: Playlists */
	get_header();?>
	
	<div class="audio-section playlists">
		<div class="row">
			<div class="medium-11 medium-centered text-center columns">
				<div class="blue-circle">
					<img src="<?php bloginfo('template_url');?>/images/blue-music-note.png">
				</div>
				<h1 class="blue-text"><?php the_title();?></h1>
				<p class="blue-text quote"><?php the_field('client_quote', 9);?></p>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 medium-centered columns">
				<div id="playlist-player" class="audio-player">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php the_content();?>
						<?php endwhile; endif;?>
				</div>
				<?php wp_nav_menu( array( 'theme_location' => 'Playlists' ) ); ?>
			</div>
		</div>
	</div>
	<div id="list" class="song-list">
		<div class="row circle-row">
			<div class="medium-11 columns medium-centered">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/bullets.png" class="bullets">
				</div>
				<h1>SONG LISTS</h1>
					<p>
						<?php the_field('client_quote_2', 9);?>
					</p>
					
					<div id="player2">
						<?php the_field('player_2', 9);?>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-6 medium-centered columns text-center">
				<a href="<?php the_field('song_list', 9);?>" target="_blank" class="pdf">
				<img src="<?php bloginfo('template_url');?>/images/down-arrow.png">
					<br><br>
				<span>DOWNLOAD SONG LIST PDF</span>
				</a>
			</div>
		</div>
	</div>
	
	<div id="videos" class="videos">
		<div class="row">
			<div class="medium-11 medium-centered columns">
				<div class="circle">
					<img src="<?php bloginfo('template_url');?>/images/video.png" class="video">
				</div>
				<h1>VIDEOS</h1>
			</div>
		</div>
		<div class="row">
			<div class="medium-6 columns">
				<div class="fluidMedia">
					<?php the_field('left_video', 9);?>
				</div>
			</div>
			<div class="medium-6 columns">
				<div class="fluidMedia">
					<?php the_field('right_video', 9);?>
				</div>
			</div>
		</div>
	</div>

	
<?php get_footer();?>