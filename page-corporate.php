<?php /*Template Name: Corporate Events */
	get_header();
	//get the featured image
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' );
	$url = $thumb['0'];
	?>
	
<div class="corp-header" style="background:url(<?=$url?>) center center no-repeat;">
	<div class="row">
		<div class="medium-10 medium-centered text-center columns">
			<h1>CORPORATE EVENTS</h1>
			<h2>SYNERGY ON THE DANCE FLOOR.</h2>
		</div>
	</div>
	<div class="corp-subheader">
		<div class="row">
			<div class="medium-11 medium-centered text-center columns">
				<p><?php the_field('quote');?></p>
				<p class="cite"><?php the_field('cite');?></p>
			</div>
		</div>
	</div>
</div>
<section class="corp-white">
	<div class="row">
		<div class="medium-12 columns">
			<?php if (have_posts()) : while (have_posts()) : the_post();
					
					
				the_content( );
					
					
				endwhile; endif;?>
		</div>
	</div>
</section>
<section class="corp-blue">
	<div class="row">
		<div class="medium-12 columns medium-centered three-col">
			<?php the_field('services');?>
		</div>
	</div>
</section>
	
<? get_footer(); ?>