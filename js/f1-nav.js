/*
------Factor1 Nav------
---------a jquery tool by Matt Adams (@mattada) and Eric Stout (@buschschwick)
-------------version 1.1
http://factor1studios.com
*/

jQuery(document).ready(function($) {
		$(document).ready(function(){
		
			$('.mm_open').click(function(){ //listen for the click on the menu icon
				$('.panel').slideDown(700).css("position", "relative"); //push down the nav											
				$('body').append('<div class="mask"></div>'); //lets create a nice mask to dim the content
			
			$('.mm_close').click(function(){ //listen for the close button click
				$('.panel').slideUp(700).css("position", "relative"); //pull up the nav
				$('.mask').removeClass('mask'); //take off that mask
				});
				
		//or alternatively you can click the mask and make that shit go away.
			$('.mask').click(function(){ //listen for the click on the mask
				$('.panel').slideUp(700).css("position", "relative"); //pull up the nav
				$('.mask').removeClass('mask'); //take off that mask
				});
			});
		});
	});