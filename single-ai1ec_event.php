<?php get_header(); ?>
<div class="row">
<article>
	<div class="medium-8 columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<h2 class="blog-title"><?php the_title(); ?></h2>
			
			
			<?php if(has_post_thumbnail()) {
			the_post_thumbnail();
			} else {	}
			?>	
			
			
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php the_tags( 'Tags: ', ', ', ''); ?>


			
			<?php edit_post_link('Edit this entry','','.'); ?>
			
		</div>



	<?php endwhile; endif; ?>
	</div>
</article>

	<div class="medium-4 columns sidebar">	
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>

